# Preparing locales
echo 'LC_ALL="en_US.UTF-8"'  >  /etc/default/locale
echo 'LC_ALL="en_US.UTF-8"'  >  /etc/default/locale

# Install kubectl
sudo apt-get update && sudo apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo touch /etc/apt/sources.list.d/kubernetes.list 
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl

# Installing kops
wget https://github.com/kubernetes/kops/releases/download/1.11.1/kops-linux-amd64
chmod +x kops-linux-amd64
sudo mv kops-linux-amd64 /usr/local/bin
sudo mv /usr/local/bin/kops-linux-amd64 /usr/local/bin/kops
sudo apt-get install python-pip -y
