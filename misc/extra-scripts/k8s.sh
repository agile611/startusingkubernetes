# Install kubernetes
apt-get update 
apt-get install -y apt-transport-https net-tools
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
apt-get update
apt-get install -y kubeadm kubelet kubectl kubernetes-cni

# Preparing locales
echo 'LC_ALL="en_US.UTF-8"'  >  /etc/default/locale
echo 'LC_ALL="en_US.UTF-8"'  >  /etc/default/locale

# kubelet requires swap off
swapoff -a
# keep swap off after reboot
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
sudo sed -i '0,/ExecStart=/s//Environment="KUBELET_EXTRA_ARGS=--cgroup-driver=cgroupfs"\n&/' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

cat > /etc/containerd/config.toml <<EOF
[plugins."io.containerd.grpc.v1.cri"]
  systemd_cgroup = true
EOF
systemctl restart containerd

# Get the IP address that VirtualBox has given this VM
IPADDR=`ifconfig enp0s8 | grep Mask | awk '{print $2}'| cut -f2 -d:`
echo This VM has IP address $IPADDR

# Set up Kubernetes
NODENAME=$(hostname -s)
kubeadm config images pull
kubeadm init --apiserver-cert-extra-sans=$IPADDR  --node-name $NODENAME

# Set up root creds for the vagrant user
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
export KUBECONFIG=/etc/kubernetes/admin.conf

#Set the node to Ready
kubectl apply -f "https://cloud.weave.works/k8s/net"
kubectl taint nodes --all node-role.kubernetes.io/control-plane:NoSchedule --overwrite
kubectl taint nodes --all node-role.kubernetes.io/control-plane:NoSchedule-
kubectl taint nodes --all node-role.kubernetes.io/master:NoSchedule --overwrite
kubectl taint nodes --all node-role.kubernetes.io/master:NoSchedule-

# Set up admin creds for the vagrant user
echo Copying credentials to /home/vagrant...
sudo --user=vagrant mkdir -p /home/vagrant/.kube
cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
chown $(id -u vagrant):$(id -g vagrant) /home/vagrant/.kube/config

#Try the setup
#kubectl apply -f https://bitbucket.org/agile611/startusingkubernetes/raw/b33c827381cde887601613c7f3ec366d26b43ee4/examples/001-pod-helloworld.yml