[![Agile611](https://www.agile611.com/wp-content/uploads/2020/09/cropped-logo-header.png)](http://www.agile611.com/)
# Agile611 Start Using Kubernetes [Practice Environment for CKA/CKAD and CKS Exams]

Here there is a working environment to start playing and learning Kubernetes.

## Tools you need to run the code

If you want to use the code from this repo, you will need the following tools:

* [git](https://git-scm.com)  
* [Visual Studio Code](https://code.visualstudio.com)  
* [Vagrant](https://www.vagrantup.com)  
* [Virtualbox](https://www.virtualbox.org)  

# Vagrantfile and Scripts to Automate Kubernetes Setup using Kubeadm 

## Documentation

Current k8s version for [CKAD](https://www.cncf.io/certification/ckad/) exam: 1.23

Refer this link for [documentation](https://devopscube.com/kubernetes-cluster-vagrant/)


## Prerequisites

1. Working Vagrant + Virtualbox Setup 
2. 16 Gig RAM Recommended workstation as the Vms use 3 vCPUS and 4+ GB RAM

## For MAC Users

Latest version of Virtualbox for Mac/Linux can cause issues because you have to create/edit the /etc/vbox/networks.conf file and add:
<pre>* 0.0.0.0/0 ::/0</pre>

or run below commands
```sh
sudo mkdir -p /etc/vbox/
echo * 0.0.0.0/0 ::/0 | sudo tee -a /etc/vbox/networks.conf
```

So that the host only networks can be in any range, not just 192.168.56.0/21 as described [here](https://discuss.hashicorp.com/t/vagrant-2-2-18-osx-11-6-cannot-create-private-network/30984/23)

 
## Usage/Examples

To provision the cluster, execute the following commands.

```shell
git clone https://bitbucket.org/agile611/startusingkubernetes.git
cd startusingkubernetes
vagrant up
```

## Potential problems in Windows 10

There are some known issues in Windows 10 when the users are starting the Vagrant environment.

Some changes must be changed in the Vagrantfile

```
    for i in 30000..30100
      k8smaster.vm.network :forwarded_port, guest: i, host: i
    end
```

## Common networking problems

If you have proxies or VPNs running on your machine, it is possible that Vagrant is not able to provision your environment.

Please check your connectivity before.

## Set Kubeconfig file variable to access the cluster from outside

```shell
cd startusingkubernetes
cd configs
export KUBECONFIG=$(pwd)/config
```

or you can copy the config file to .kube directory.

```shell
cp config ~/.kube/
```

## Kubernetes Metrics 

In the cluster, we have to add a value on a config to get the metrics. We have to add hostNetwork: true below DnsNetworkPolicy

```shell
kubectl edit deployments.apps -n kube-system metrics-server
```

[![DnsNetWork](https://i.stack.imgur.com/8nOBy.png)](https://stackoverflow.com/questions/62138734/metric-server-not-working-unable-to-handle-the-request-get-nodes-metrics-k8s)


## Check the cluster

Execute these commands to check the cluster

```shell
kubectl get nodes
kubectl apply -f https://bitbucket.org/agile611/startusingkubernetes/raw/b33c827381cde887601613c7f3ec366d26b43ee4/examples/001-pod-helloworld.yml
```
## To shutdown the cluster

```shell
vagrant halt
```

## To restart the cluster

```shell
vagrant up
```

## To destroy the cluster

```shell
vagrant destroy -f
```

# CKAD Exercises

A set of exercises that helped me prepare for the [Certified Kubernetes Application Developer](https://www.cncf.io/certification/ckad/) exam, offered by the Cloud Native Computing Foundation, organized by curriculum domain. They may as well serve as learning and practicing with Kubernetes.

During the exam, you are allowed to keep only one other browser tab open to refer to official documentation. Make a mental note of the breadcrumb at the start of the excercise section, to quickly locate the relevant document in kubernetes.io. It is recommended that you read the official documents before attempting exercises below it.

## Contents from the official CKAD Exam Curriculum
- 20% - Application Design and Build
    * Define, build and modify container images
    * Understand Jobs and CronJobs
    * Understand multi-container Pod design patterns (e.g. sidecar, init and others)
    * Utilize persistent and ephemeral volumes
- 20% - Application Deployment
    * Use Kubernetes primitives to implement common deployment strategies (e.g. blue/ green or canary)
    * Understand Deployments and how to perform rolling updates
    * Use the Helm package manager to deploy existing packages
- 15% - Application observability and maintenance
    * Understand API deprecations
    * Implement probes and health checks
    * Use provided tools to monitor Kubernetes applications
    * Utilize container logs
    * Debugging in Kubernetes
- 25% - Application Environment, Configuration and Security
    * Discover and use resources that extend Kubernetes (CRD)
    * Understand authentication, authorization and admission control
    * Understanding and defining resource requirements, limits and quotas
    * Understand ConfigMaps
    * Create & consume Secrets
    * Understand ServiceAccounts
    * Understand SecurityContexts
- 20% - Services & Networking
    * Demonstrate basic understanding of NetworkPolicies
    * Provide and troubleshoot access to applications via services
    * Use Ingress rules to expose applications

## Examples to prepare CKAD
- [Core Concepts](https://bitbucket.org/agile611/startusingkubernetes/src/master/ckad/a.core_concepts.md)
- [Multi-container pods](https://bitbucket.org/agile611/startusingkubernetes/src/master/ckad/b.multi_container_pods.md)
- [Pod design](https://bitbucket.org/agile611/startusingkubernetes/src/master/ckad/c.pod_design.md)
- [Configuration](https://bitbucket.org/agile611/startusingkubernetes/src/master/ckad/d.configuration.md)
- [Observability](https://bitbucket.org/agile611/startusingkubernetes/src/master/ckad/e.observability.md)
- [Services and networking](https://bitbucket.org/agile611/startusingkubernetes/src/master/ckad/f.services.md)
- [State persistence](https://bitbucket.org/agile611/startusingkubernetes/src/master/ckad/g.state.md)
- [helm](https://bitbucket.org/agile611/startusingkubernetes/src/master/ckad/h.helm.md)
- [Custom Resource Definitions](https://bitbucket.org/agile611/startusingkubernetes/src/master/ckad/i.crd.md)
- [Bonus](https://bitbucket.org/agile611/startusingkubernetes/src/master/ckad/j.bonus.md)

# More Certified Kubernetes Application Developer (CKAD) Preparation Course Content

In this learning path, we are going to explore the topics covered in the [CKAD exam](https://www.cncf.io/certification/ckad/) to fully prepare you to pass the certification exam. You’ll look at determining when and how you should apply the core concepts of Kubernetes to manage an application. You’ll also examine the kubectl command-line tool, a mainstay of the Kubernetes engineer. Ben also offers tips to help you better prepare for the exam and shares his personal experience with getting ready for all aspects of the exam.

# Support

This tutorial is released into the public domain by [Agile611](http://www.agile611.com/) under Creative Commons Attribution-NonCommercial 4.0 International.

[![License: CC BY-NC 4.0](https://img.shields.io/badge/License-CC_BY--NC_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc/4.0/)


This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhs/) and is likewise released into the public domain.

Please contact Agile611 for further details.

* [Agile611](http://www.agile611.com/)
* Laureà Miró 309
* 08950 Esplugues de Llobregat (Barcelona)

[![Agile611](https://www.agile611.com/wp-content/uploads/2020/09/cropped-logo-header.png)](http://www.agile611.com/)
