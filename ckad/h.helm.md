# Managing Kubernetes with Helm

- Note: Helm is part of the new CKAD syllabus. Here are a few examples of using Helm to manage Kubernetes.

## Helm in K8s

### Install Helm

Install helm in the master using the following script:
```bash
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm -y
```
[Install a chart from 0](https://opensource.com/article/20/5/helm-charts)

[Example url nodejs](https://artifacthub.io/packages/helm/banzaicloud-stable/nodejs)

### Install Wordpress using Helm

```bash
helm list --all
```

```bash
helm search hub wordpress
```

```bash
helm repo add stable https://charts.helm.sh/stable --force-update
```

```bash
helm repo add stable https://charts.helm.sh/stable --force-update
```

```bash
helm search repo stable | grep wordpress
```

```bash
helm install stable/wordpress --generate-name
```

### Creating a basic Helm chart

```bash
helm create chart-test ## this would create a helm 
```

### Running a Helm chart

```bash
helm install -f myvalues.yaml my redis ./redis
```

### Find pending Helm deployments on all namespaces

```bash
helm list --pending -A
```

### Uninstall a Helm release

```bash
helm uninstall -n namespace release_name
```

### Upgrading a Helm chart

```bash
helm upgrade -f myvalues.yaml -f override.yaml redis ./redis
```

### Using Helm repo

Add, list, remove, update and index chart repos

```bash
helm repo add [NAME] [URL]  [flags]

helm repo list / helm repo ls

helm repo remove [REPO1] [flags]

helm repo update / helm repo up

helm repo update [REPO1] [flags]

helm repo index [DIR] [flags]
```

### Download a Helm chart from a repository 

```bash
helm pull [chart URL | repo/chartname] [...] [flags] ## this would download a helm, not install 
helm pull --untar [rep/chartname] # untar the chart after downloading it 
```
